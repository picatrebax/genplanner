# Genplanner: Nice Planners or Calendars for Print and More  #

## Quick Summary ##

Scripts and templates to build calendar planner LaTeX files. The output is to be
processed by LaTeX (pdflatex, lualatex and such) to produce pdf files with
desired planners. It supports different styles and layouts. User can modify
standard styles or create their own styles.

## Motivation ##

I created a few Python scripts that generate very nice calendars that I used for
years. I printed them instead of standard daily planners, I printed them for
family calendars to place on a refridgerator and so on. I have never regretted a
few days I spent for programming my scripts as benefits were so great for me. It
is not obvious that they may be useful for anybody else as people now use
computers and cell phones more than pens, which I think is unfortunate. However,
there maybe some people who would benefit from my work, so I leave it here.

## Build Status ##

This is a working version of genplanner.

## Version ##

1.0.0

### How do I get set up? ###

#### Installation

Use pip:

```console
pip install genplanner
```

#### Dependencies

python3, suggeested pip for installation.

LaTeX with tikz package installed (tested with texlive: pdflatex, lualatex).

For installation from the source code you may need to have pip, setuptools,
wheel, build:

```console
python3 -m pip install --upgrade pip setuptools wheel
```

Another system that need to be installed:

```console
python3 -m pip install --upgrade build
```

### Usage ###

#### Getting help

Use ``genplanner --help`` to see all options and usage suggestions.

Use ``genplanner --version`` to check the version number.

#### Some examples:

##### Nice planner with default style on a4 page in Landscape mode for August 2022:

```console
genplanner --landscape --paper=a4 -f Monday 2022 8
```

This will produce file "planner.tex" which needs to be processed with your LaTeX
processor, for example:

```console
pdflatex planner.tex
```

##### A planner with two calendars per page in raspberry style

```console
genplanner -S raspberry -m2 -p2 2022 8
pdflatex planner.tex
```

##### Kiwi style with solid lines

```console
genplanner -S kiwi -L frames --frames -m2 -p2 2022 8
pdflatex planner.tex
```

##### Alternating style and layout options

It is easy to alternate any default style. For example, create a JSON file:
"overrider.json" with the following content

"overrider.json"=
```json
{
    "NumberDX":          "0",
    "NumberDY":          "0",

    "TextNumberMondayColor":"f4f6f6",
    "TextNumberTuesdayColor": "f4f6f6",
    "TextNumberWednesdayColor": "f4f6f6",
    "TextNumberThursdayColor": "f4f6f6",
    "TextNumberFridayColor": "f4f6f6",
    "TextNumberSaturdayColor": "fdf2e9",
    "TextNumberSundayColor": "fdf2e9",

    "TextNumberFontCmd": "\\Huge",
    "TitleBoxFontCmd": "\\bfseries\\Huge",
    "NameCellFontCmd": "\\Large",
    "MTitleBoxHeight":   "9mm"
}
```

Then use the command to generate a new calendar with large numbers for days in
the middle of the day cells, and with bolder fonts.

```console
genplanner -S kiwi -m2 -p2 --SR=overrider.json --LR=overrider.json
pdflatex planner.tex
```

##### Adding meta information to the calendar

To print on the last page of the calendar the settings you used for its
generation, use ``--meta-info`` option. It can be very useful to be able to
repeat the same planner generation.

To generate the planner for the whole 2023 year with Meta information included
in smoothie style with 1 calendar per page in landscape mode, use the following
command:

```console
genplanner -S smoothie -m12 -p1 --landscape --meta-info 2023 1
pdflatex planner.tex
```

### Contribution guidelines ###

#### Writing styles and layouts

Todo.

To add a style or layout add JSON file with it into according directory:
``src/genplanner/data/...``

#### Other guidelines

The code is in ``genplanner.py`` file mainly and in many LaTeX templates.

### Who do I talk to? ###

* Repo owner or admin

Anton Kulchitsky

* Other community or team contact

Pica Trebax, Inc. is a good contact to reach the author: [https://www.picatrebax.com](https://www.picatrebax.com)

# Emacs #
Local Variables:
mode: markdown
eval: (visual-line-mode)
End:
