# genplanner - nice calendars/planners for print and more
#
# Copyright (c) 2021-2022 Anton V. Kulchitsky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>

import os
from string import Template

class CLTemplates:
    '''
    Class storing all the templates as a dictionary of strings
    '''

    def __init__(self, tpldict, datadir):
        '''
        Initialization of the template list by directory and files

        Args:
            tpldict: dictionary with the match of template names and files
            datadir: default data directory
        '''
        self.tpldir = os.path.join( datadir, tpldict['directory'] )
        self.fnames = tpldict['files']
        # maybe add a check that all templates are in place

    def get(self, name):
        '''
        Returns a template by its name
        '''
        return Template(
            open(os.path.join(self.tpldir,
                              self.fnames[name])).read() )

