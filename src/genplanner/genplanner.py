#!/usr/bin/env python3
#
# genplanner - nice calendars/planners for print and more
#
# Copyright (c) 2021-2022 Anton V. Kulchitsky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>

'''
genplanner main executable file
'''

import json
import argparse
import os, sys
import glob
from string import Template
from importlib import metadata

import calendar
calendar_fdowList = { "Monday": calendar.MONDAY,
                      "Tuesday": calendar.TUESDAY,
                      "Wednesday": calendar.WEDNESDAY,
                      "Thursday": calendar.THURSDAY,
                      "Friday": calendar.FRIDAY,
                      "Saturday": calendar.SATURDAY,
                      "Sunday": calendar.SUNDAY }

from .cltemplates import CLTemplates  # local class for reading and storing templates

DEFAULTS_NAME    = 'genplanner'
DEFAULTS_RUNDIR  = os.path.dirname(__file__)
DEFAULTS_DATADIR = os.path.join(DEFAULTS_RUNDIR, 'data')
DEFAULTS_FNAME   = os.path.join(DEFAULTS_DATADIR, 'defaults.json')
__version__ = open(os.path.join(DEFAULTS_RUNDIR,'VERSION')).read()

def json_read(fname = DEFAULTS_FNAME):
    '''
    Reads default values for different cases

    Args:
        fname: file name with default values (defaults.json)

    Returns:
        Dictionary of read json default values
    '''
    f = open(fname)
    data = json.load(f)
    f.close()

    return data

def basenames_list_by_extension( fpath, extension ):
    '''
    Creates a list of base names of files described by directory path and
    extension.

    Args:
        fpath: directory with files
        extension: files extension

    Return:
        List of base names by path and extension

    '''
    pattern = '*.{}'.format(extension)
    length = 1 + len(extension)
    fpath  = os.path.join(fpath, pattern)
    return [ os.path.basename(a)[:-length] for a in glob.glob(fpath) ]


def optread(defaults):
    '''
    Reading and parsing program arguments

    Args:
        defaults: Dictionary of default values read by "defaults_read"

    Return:
        Dictionary like option object with all arguments filled
    '''

    argdefaults = defaults['args']
    psize_choices = defaults['psize_choices'] # LaTeX geometry package paper size list

    parser = argparse.ArgumentParser()
    parser.add_argument( 'year',  nargs='?', type = int,
                         help = 'calendar starting year', default = 1972 )
    parser.add_argument( 'month', nargs='?', type = int,
                         help = 'calendar starting month', default = 8 )

    parser.add_argument( '--version', action='version', version=__version__ )
    
    parser.add_argument( '--output', '-o',
                         help = 'output LaTeX file (default: {})'
                         .format(argdefaults['output']),
                         default=argdefaults['output'] )

    parser.add_argument('--months', '-m',
                        type = int,
                        help = 'months total (default: {})'
                        .format(argdefaults['months']),
                        default=argdefaults['months'])

    parser.add_argument('--months-per-page', '-p',
                        type = int,
                        choices = [1,2,3,4],
                        help = 'months on each page (default: {})'
                        .format(argdefaults['months_per_page']),
                        default=argdefaults['months_per_page'])

    parser.add_argument('--first-day-of-week', '-f',
                        choices = calendar_fdowList.keys(),
                        help = 'first day of week (default: {})'
                        .format(argdefaults['fdow']),
                        default=argdefaults['fdow'])

    # list of paper sizes read
    parser.add_argument('--paper', '-X',
                        choices = psize_choices.keys(),
                        help = 'paper size (default: {})'
                        .format(argdefaults['paper']),
                        default=argdefaults['paper'])

    parser.add_argument('--paperwidth',
                        help = 'paper width (overrides --paper)')

    parser.add_argument('--paperheight',
                        help = 'paper height (overrides --paper)')
    
    parser.add_argument('--landscape', action='store_true',
                        help = 'landscape mode (default: {})'
                        .format(argdefaults['landscape']),
                        default=argdefaults['landscape'])
    parser.add_argument('--ak', action='store_true',
                        help = argparse.SUPPRESS )
    
    parser.add_argument('--lmargin',
                        help = 'paper left margin (default: {})'
                        .format(argdefaults['lmargin']),
                        default=argdefaults['lmargin'])
    parser.add_argument('--rmargin',
                        help = 'paper right margin (default: {})'
                        .format(argdefaults['rmargin']),
                        default=argdefaults['rmargin'])

    parser.add_argument('--tmargin',
                        help = 'paper top margin (default: {})'
                        .format(argdefaults['tmargin']),
                        default=argdefaults['tmargin'])

    parser.add_argument('--bmargin',
                        help = 'paper bottom margin (default: {})'
                        .format(argdefaults['bmargin']),
                        default=argdefaults['bmargin'])

    # list of languages (list of files with languages in data/language)
    langdir = os.path.join( DEFAULTS_DATADIR,
                            defaults['language']['directory'] )

    # list of files without .json extension gives languages
    flist = basenames_list_by_extension( langdir, 'json' )

    parser.add_argument( '--language', '-l',
                         choices = flist,
                         help = 'language (default: {})'
                         .format(argdefaults['language']),
                         default=argdefaults['language'] )

    # list of languages (list of files with languages in data/language)
    layoutdir = os.path.join( DEFAULTS_DATADIR,
                              defaults['layout']['directory'] )

    # list of files without .json extension gives languages
    flist = basenames_list_by_extension( layoutdir, 'json' )

    parser.add_argument( '--layout', '-L',
                         choices = flist,
                         help = 'layout (default: {})'
                         .format(argdefaults['layout']),
                         default=argdefaults['layout'] )

    # list of languages (list of files with languages in data/language)
    styledir = os.path.join( DEFAULTS_DATADIR,
                             defaults['style']['directory'] )

    # list of files without .json extension gives languages
    flist = basenames_list_by_extension( styledir, 'json' )

    parser.add_argument( '--style', '-S',
                         choices = flist,
                         help = 'style (default: {})'
                         .format(argdefaults['style']),
                         default=argdefaults['style'] )

    # style correction file name
    parser.add_argument( '--style-override', '--SR',
                         help = 'style override file to correct the style' )

    # layout correction file name
    parser.add_argument( '--layout-override', '--LR',
                         help = 'layut override file to correct the style' )

    # frame override of styles
    parser.add_argument( '--frames', action='store_true', default=False,
                         help = 'Override variables in styles to draw lines' )
    
    # meta information on the last page
    parser.add_argument( '--meta-info', action='store_true', default=False,
                         help = 'print meta information on the last page' )

    result_args = parser.parse_args()
    args_analyze( result_args )

    return result_args

def month_content( year, month, cltemplates, layoutconf, is_last ):
    '''
    Returns a string with LaTeX content of the month of the year.

    Args:
        year: year
        month: month
        cltemplates: object with LaTeX templates
        layoutconf: configuration for layout (for intermonth space)
        is_last: True if this is the last month

    Return:
        String of table content for the whole month to be inserted into the
        table of that month of the year.
    '''
    first_day_of_week = calendar.firstweekday() # 0=Monday
    dayslist = list( calendar_fdowList.keys() )
    matrix_cal = calendar.monthcalendar(year, month)

    # Calendar header: days of week names
    namecell_tpl    = cltemplates.get('namecell')
    namecellrow_tpl = cltemplates.get('namecellrow')

    namecells = ""
    for N in range(7):
        DOW = ( first_day_of_week + N ) % 7
        namecells += namecell_tpl.substitute( N = N,
                                              DOW = dayslist[DOW] )

    namecellrow = namecellrow_tpl.substitute( NAMECELLS = namecells )

    # Calendar day rows
    daycell_tpl     = cltemplates.get('daycell')
    emptycell_tpl   = cltemplates.get('emptycell')
    daycellrow_tpl  = cltemplates.get('daycellrow')

    daycells = ""
    for week in matrix_cal:
        N = 0
        row = ""
        for day in week:
            DOW = ( first_day_of_week + N ) % 7
            if not day:
                row += emptycell_tpl.substitute( N = N )
            else:
                row += daycell_tpl.substitute(
                    N = N, DAYN = day, DOW = dayslist[DOW] )
            N += 1
        daycells += daycellrow_tpl.substitute( ROW = row )

    # title generation
    title_tpl =  cltemplates.get('monthtitle')
    title = title_tpl.substitute(
        # replace with command
        MONTH = '{{\\{}}}'.format(calendar.month_name[month]),
        YEAR = year )

    # between months addition
    intermonth_tpl = cltemplates.get('intermonth')
    if is_last:
        intermonth = ""
    else:
        intermonth = intermonth_tpl.substitute( layoutconf )

    # month as a whole
    month_tpl =  cltemplates.get('month')

    month = month_tpl.substitute(
        MONTHTITLE = title,
        NAMECELLS = namecellrow,
        DAYCELLS = daycells,
        NMROWS = len(matrix_cal),
        INTERMONTH = intermonth)

    return month

def next_month( year, month ):
    '''
    Returns next pair of year, month after the current. e.g 2007 7 will follow
    2007 8 etc.

    Args:
        year: year
        month: month

    Return:
        year, month for the next month

    '''
    if month == 12: return year+1, 1
    else: return year, month+1

def read_styleconf(args, defaults, cltemplates):
    '''
    Reads style configuration from the style JSON file

    Args:
        args: argument namespace with arguments read by optparser
        defaults: Dictionary of default values read by "defaults_read"
        cltemplates: object with LaTeX templates

    Return:
        Dictionary with styles for style template
    '''

    fstyle = os.path.join(DEFAULTS_DATADIR,defaults['style']['directory'],
                           '{}.json'.format(args.style))
    styleconf = json_read(fstyle)

    if args.style_override:
        styleconf.update(json_read(args.style_override))

    if args.frames:
        dname_over = os.path.join(DEFAULTS_DATADIR,defaults['overrides']['directory'])
        fname_over = os.path.join(dname_over, defaults['overrides']['frames'])
        styleconf.update(json_read(fname_over))

    styleconf['FontPackageCommand']='' # default no string
    if styleconf['FontPackage']:
        styleconf['FontPackageCommand'] = \
            '\\usepackage{{{}}}'.format(styleconf['FontPackage'])

    # table of Font Family matching with LaTeX commands
    stylematch = { 'mono' : '\\ttdefault',
                   'sans' : '\\sfdefault',
                   'serif': '\\rmdefault' }
    styleconf['FontFamilyCommand'] = stylematch[ styleconf['FontFamily'] ]

    if styleconf['NameCellFrameColor']:
        styleconf['NameCellFrameColorCmd'] =\
            'draw={},'.format('NameCellFrameColor')
    else:
        styleconf['NameCellFrameColorCmd'] = '%%'
        styleconf['NameCellFrameColor'] = 'ffffff'

    if styleconf['DayCellFrameColor']:
        styleconf['DayCellFrameColorCmd'] =\
            'draw={},'.format('DayCellFrameColor')
    else:
        styleconf['DayCellFrameColorCmd'] = '%%'
        styleconf['DayCellFrameColor'] = 'ffffff'

    return styleconf

def read_layoutconf(args, defaults, cltemplates):
    '''
    Reads layout configuration from the layout JSON file

    Args:
        args: argument namespace with arguments read by optparser
        defaults: Dictionary of default values read by "defaults_read"
        cltemplates: object with LaTeX templates

    Return:
        Dictionary with layout for layout LaTeX template
    '''
    
    flayout = os.path.join(DEFAULTS_DATADIR,defaults['layout']['directory'],
                           '{}.json'.format(args.layout))
    layoutconf = json_read(flayout)

    if args.layout_override:
        layoutconf.update(json_read(args.layout_override))

    # for auto computations of cell height
    layoutconf['MonthsPerPage'] = args.months_per_page
    # maximum number of calendar rows per months per page
    maxrows = [6, 11, 16, 22]
    # maximum number of intercell rows per months per page
    maxinters = [5, 9, 13, 18]

    layoutconf['CalRows']     = maxrows[args.months_per_page-1]
    layoutconf['InterCellDs'] = maxinters[args.months_per_page-1]

    # processing conditional automax option for cell width
    if layoutconf['DayCellWidth'] == 'auto':
        layoutconf['DayCellWidth'] = 0
        layoutconf['IsAutoDayCellWidth'] = 1
    else:
        layoutconf['IsAutoDayCellWidth'] = 0

    # processing conditional automax option for cell height
    if layoutconf['DayCellHeight'] == 'auto':
        layoutconf['DayCellHeight'] = 0
        layoutconf['IsAutoDayCellHeight'] = 1
    else:
        layoutconf['IsAutoDayCellHeight'] = 0

    # processing conditional automax option for name cell width
    if layoutconf['NameCellWidth'] == 'auto':
        layoutconf['NameCellWidth'] = 0
        layoutconf['IsAutoNameCellWidth'] = 1
    else:
        layoutconf['IsAutoNameCellWidth'] = 0

    return layoutconf

def read_geomconf(args, defaults):
    '''
    Reads paper geometry configuration from arguments

    Args:
        args: argument namespace with arguments read by optparser
        defaults: Dictionary of default values read by "defaults_read"

    Return:
        Dictionary with paper size settings

    '''
    psize_choices = defaults['psize_choices']

    geomconf = { 'paper':    psize_choices[args.paper],
                 'tmargin':  args.tmargin,
                 'bmargin':  args.bmargin,
                 'lmargin':  args.lmargin,
                 'rmargin':  args.rmargin,
                 'landscape': str(args.landscape) }

    if args.paperwidth:
        geomconf['paperwidthcmd'] = '\\paperwidth={},'.format(args.paperwidth)
    else:
        geomconf['paperwidthcmd'] = '%%'
        
    if args.paperheight:
        geomconf['paperheightcmd'] = '\\paperheight={},'.format(args.paperheight)
    else:
        geomconf['paperheightcmd'] = '%%'


    return geomconf

def read_langconf(args,defaults, cltemplates):
    '''
    Reads language configuration from the language JSON file

    Args:
        args: argument namespace with arguments read by optparser
        defaults: Dictionary of default values read by "defaults_read"
        cltemplates: object with LaTeX templates

    Return:
        Dictionary with language substitutions
    '''

    flang = os.path.join(DEFAULTS_DATADIR,defaults['language']['directory'],
                           '{}.json'.format(args.language))
    langconf = json_read(flang)

    return langconf

def read_metaconf(args, defaults, cltemplates):
    '''
    Creates meta configuration for writing into pdf file or on the planner

    Args:
        args: argument namespace with arguments read by optparser
        defaults: Dictionary of default values read by "defaults_read"
        cltemplates: object with LaTeX templates

    Return:
        Dictionary with meta substitutions
    '''
    metaconf = {'PDFTITLE': 'Planner {} {}'.format(args.year, args.month),
                'PDFSUBJECT': '',
                'PDFAUTHOR': '{} ver.{}'.format(DEFAULTS_NAME, __version__),
                'PDFKEYWORDS': '' }

    if args.meta_info:
        minfo = '{} {}, style: {}, layout: {}, -m{}, -p{}, -X{}, margins[LTRB]: {},{},{},{}'\
            .format(
                DEFAULTS_NAME, __version__, args.style,
                args.layout, args.months, args.months_per_page, args.paper,
                args.lmargin, args.tmargin, args.rmargin, args.bmargin )
        metaconf['LASTFOOTNOTE'] = minfo
    else:
        metaconf['LASTFOOTNOTE'] = ''
    
    return metaconf

def args_analyze(args):
    '''
    Performs argument analysis and does some actions required

    Args:
        args: argument namespace with arguments read by optparser

    '''
    if args.ak:
        print( 'The Moon is up and yet it isn\'t night' )
        print( 'Sunset divides the sky with her' )
        sys.exit(0)

def main():
    '''
    Entry point. Main executable function

    Return: 
        Error code, 0 on success
    '''

    defaults = json_read()
    args     = optread( defaults )

    # template processing class
    cltemplates = CLTemplates(defaults['templates'],DEFAULTS_DATADIR)

    # reading layout
    layoutconf = read_layoutconf(args, defaults, cltemplates)
    layout = cltemplates.get('layout').substitute(layoutconf)

    # reading style
    styleconf = read_styleconf(args, defaults, cltemplates)
    style = cltemplates.get('style').substitute(styleconf)

    # reading geometry
    geomconf = read_geomconf(args, defaults)
    geometry = cltemplates.get('geometry').substitute(geomconf)
    
    # Language file read
    langconf = read_langconf(args, defaults, cltemplates)
    language = \
        cltemplates.get('language').substitute(langconf,
                                               Language=args.language.lower())

    # Meta info file read
    metaconf = read_metaconf(args, defaults, cltemplates)
    metainfo = cltemplates.get('metainfo').substitute(metaconf)

    # generation monthly calendar content
    calendar.setfirstweekday(calendar_fdowList[args.first_day_of_week])

    content = ""
    year = args.year
    month = args.month

    for m in range(args.months):
        content += month_content( year, month, cltemplates,
                                  layoutconf, m == args.months-1 )
        year, month = next_month( year, month )

    # base fill
    base_tpl =  cltemplates.get('base')
    base = base_tpl.substitute( PLANNER = content,
                                LAYOUT = layout,
                                STYLE = style,
                                GEOMETRY = geometry,
                                LANGUAGE = language,
                                METAINFO = metainfo )

    # saving the Planner to the file
    fplanner = open(args.output,'w')
    fplanner.write(base)
    fplanner.close()

    return 0
