# Developer's guide #

This is just a short helper file.

## Developer's evnironment ##

Some of the following material is taken from
https://packaging.python.org/guides/distributing-packages-using-setuptools/

and

https://setuptools.pypa.io/en/latest/index.html

The latest link describing setuptools used for packaging.

### Requirements for installation and development ###

Installing requirements with all necessary packages are available here:

https://packaging.python.org/tutorials/installing-packages/#installing-requirements

1. Set up python-is-python3 in package manager (Ubuntu package)
2. Install python3-pip (Ubuntu package also)
3. Install python3-testresources (Ubuntu package), needed for the next step
4. While pip alone is sufficient to install from pre-built binary archives, up
to date copies of the setuptools and wheel projects are useful to ensure you can
also install from source archives: Unix/macOS

```console
python3 -m pip install --upgrade pip setuptools wheel
```

Another system that need to be installed:

```console
python3 -m pip install --upgrade build
```

(direct call for pip is also allowed)

### Development mode ###

You can install a project in "editable" or "develop" mode while you’re working
on it. When installed as editable, a project can be edited in-place without
reinstallation: changes to Python source files in projects installed as editable
will be reflected the next time an interpreter process is started.

To install a Python package in "editable"/"development" mode Change directory to
the root of the project directory and run:

python -m pip install -e .

The pip command-line flag -e is short for --editable, and . refers to the
current working directory, so together, it means to install the current
directory (i.e. your project) in editable mode. This will also install any
dependencies declared with install_requires and any scripts declared with
console_scripts. Dependencies will be installed in the usual, non-editable mode.

## Styles and Layouts Development ##

### Layouts ###

Positions and sizes of calendars and their elements are defined by
layout. Layout is determined by json configuration located in "layout"
directory. Layout define each month configuration and the distance between
months if they are located on the same page.

The elements of the layout are

* MTitle --  the Month Name Box
* NameCell -- the cell with week day name
* DayCell -- the day cell with day number
* InterMonthVSpace -- distance between months

Almost all parameters are defined as distance measures in TeX: in mm, in,
etc. There are a few exceptions:

* NumberDX and NumberDY are float numbers that determine a relative positioning
  of the day number in the DayCell. (0,0) is the center of the cell. The number
  is relative to the cell size. Thus, (0.5,0.5) determines the upper right
  corner.
  
* DayCellWidth, DayCellHeight, and NameCellWidth can be defined as distances or
  have a special value: "auto". "auto" for DayCell size means maximum size
  allowed to fill the page. "auto" for NameCellWidth means the same size as
  DayCell. It is a good idea generally to have these values as "auto".
  
All parameters of the layout are explained in pdf file: "layout-explained.pdf"
located in the same doc directory as this file.

### Styles ###

Styles determine colors and fonts. Styles are located in "styles"
directory. Most of the parameters are self explanatory. Colors have to be
determined as HTML colors.

### Altering default styles and layouts ###

Any parameter in styles and layout can be altered or overriden. Suce parameters
can be listed in a user defined JSON file and then this file can be passed to
genplanner with --style-override and --layout-override options.

## Program structure ##

Program itself is very self explanatory. Default values are defined in
defaults.json files. 

## LaTeX templates ##

The program builts LaTeX file from the hierarchy of the templates. All the
hierarchy is well described in the file "templates-explained.pdf" located in the
same directory as this file.
